<?php

namespace BackupMigrateAWSS3\Drupal\Destination;

use BackupMigrateAWSS3\Core\Destination\AmazonS3Destination;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Site\Settings;

/**
 * Class DrupalAWSS3Destination.
 *
 * @package BackupMigrate\Drupal\Destination
 */
class DrupalAWSS3Destination extends AmazonS3Destination {
  use MessengerTrait;

  /**
   * Init configurations.
   */
  public function configSchema($params = []) {
    $schema = [];
    // Init settings.
    if ($params['operation'] == 'initialize') {
      $schema['fields']['host'] = [
        'type' => 'text',
        'title' => t('Host'),
        'required' => TRUE,
        'description' => t('Enter Host name. For e.g. <i>s3.amazonaws.com</i>'),
      ];
      $schema['fields']['s3_bucket'] = [
        'type' => 'text',
        'title' => t('S3 Bucket'),
        'required' => TRUE,
        'description' => t('This bucket must already exist. It will not be created for you.'),
      ];
      $schema['fields']['region'] = [
        'type' => 'text',
        'title' => t('Region'),
        'required' => TRUE,
        'description' => t('Enter region. For e.g. <i>ap-south-1</i>'),
      ];
      $aws_key = Settings::get('backup_migrate_aws_access_key');
      $aws_secret = Settings::get('backup_migrate_aws_secret_key');
      if (!isset($aws_key) || !isset($aws_secret)) {
        $this->messenger()->addWarning('Please store Access Key Id and Secret Access Key in settings.php. Add access key like $settings[\'backup_migrate_aws_access_key\'] = key and $settings[\'backup_migrate_aws_secret_key\'] = secret.');

      }
    }
    return $schema;
  }

}
