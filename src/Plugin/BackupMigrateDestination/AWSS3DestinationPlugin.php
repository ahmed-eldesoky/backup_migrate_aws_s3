<?php

namespace Drupal\backup_migrate_aws_s3\Plugin\BackupMigrateDestination;

use BackupMigrate\Drupal\EntityPlugins\DestinationPluginBase;

/**
 * Defines a file directory destination plugin.
 *
 * @BackupMigrateDestinationPlugin(
 *   id = "AWSS3",
 *   title = @Translation("Amazon S3"),
 *   description = @Translation("Back up to a Amazon S3."),
 *   wrapped_class = "BackupMigrateAWSS3\Drupal\Destination\DrupalAWSS3Destination"
 * )
 */
class AWSS3DestinationPlugin extends DestinationPluginBase {
}
