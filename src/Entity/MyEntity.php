<?php

namespace Drupal\backup_migrate_aws_s3\Entity;

use Drupal\backup_migrate_aws_s3\MyentityInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the advertiser entity.
 *
 * @ingroup myentity
 *
 * @ConfigEntityType(
 *   id = "myentity",
 *   label = @Translation("My Entity"),
 *   handlers = {
 *     "list_builder" = "BackupMigrateAWSS3\Drupal\Controller\MyentityListBuilder",
 *     "form" = {
 *       "add" = "BackupMigrateAWSS3\Drupal\Form\MyEntityForm",
 *     }
 *   },
 *   config_prefix = "myentity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label"
 *   }
 * )
 */

class MyEntity extends ConfigEntityBase implements MyentityInterface {

  public $id;
  public $text;
}
