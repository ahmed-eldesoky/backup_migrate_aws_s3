<?php

namespace BackupMigrateAWSS3\Drupal\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the content_entity_example entity edit forms.
 *
 * @ingroup content_entity_example
 */
class MyEntityForm extends EntityForm {

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\content_entity_example\Entity\Contact */
    $form = parent::form($form, $form_state);

    $entity = $this->entity;

    $form['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#maxlength' => 255,
        '#default_value' => $entity->label(),
        '#description' => $this->t("Label for the my entity Example."),
        '#required' => TRUE,
    ];
    $form['id'] = [
        '#type' => 'machine_name',
        '#default_value' => $entity->id(),
        '#machine_name' => [
            'exists' => [$this, 'exist'],
        ],
        '#disabled' => !$entity->isNew(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = $entity->save();

    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label entity.', [
          '%label' => $entity->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label entity was not saved.', [
          '%label' => $entity->label(),
      ]), MessengerInterface::TYPE_ERROR);
    }

    $form_state->setRedirect('backup_migrate_aws_s3.myentity.collection');
  }
}